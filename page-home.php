<?php
/*
 Template Name: Home Page
*/
?>
<?php get_header(); ?>
<?php 
       
    // CODE to help figure out which section is displaying
       // This check the url and make sure the elements switch..
    $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    $switchURL = 'wp';
    if ((strpos($url,'uwc') !== false) ||(strpos($url,'wc') !== false)) {

        $switchURL = 'uwc';
    } elseif (strpos($url,'esl') !== false) {
        $switchURL = 'esl';
    } else {
        $switchURL = 'wp';
    }


        if ( is_front_page() ) {
        // FrontPage Display...
?>
			<div id="main-content" role="main">
				<?php // If set to Slider
					if(get_field('hero_type', 'option') == "single") { ?>
					<div id="slider">
						<ul id="bxslider">
							<?php if( have_rows('menu_items') ): ?>
							<?php while( have_rows('menu_items') ): the_row(); ?>
							<?php
								$slider_title = get_sub_field('title');
								$slider_description = get_sub_field('title_acronym');
								$slider_link = get_sub_field('link');
								$silder_image = get_sub_field('image');                                                                      
								$slider_icon = get_sub_field('icon');
								if( !empty($silder_image) ): 
									// vars
									$url = $silder_image['url'];
									$title = $silder_image['title'];
									// thumbnail
									$size = 'home-hero';
									$slide = $silder_image['sizes'][ $size ];
									$width = $silder_image['sizes'][ $size . '-width' ];
									$height = $silder_image['sizes'][ $size . '-height' ];
								endif;
                                                                      
                                if( !empty($slider_icon) ): 
									// vars
									$url = $slider_icon['url'];
									$title = $slider_icon['title'];
									// thumbnail
									$size = 'large-icons';
									$icon = $slider_icon['sizes'][ $size ];
									$width = $slider_icon['sizes'][ $size . '-width' ];
									$height = $slider_icon['sizes'][ $size . '-height' ];
								endif;
							?>		
							<?php if( $slider_link ): ?>
							<a href="<?php echo $slider_link; ?>" class="hero-link">
							<?php endif; ?>
							<li class="<?php echo $slider_description; ?>"> 
                                <div class="b-img" style="background-image: url('<?php echo $slide; ?>');">
                                    <?php if( $icon ): ?>
										<div class="icon"><img src="<?php echo $icon; ?>" /></div>
										<?php endif; ?>
								<div class="content">
									<div class="slider-content">
										<?php if( $slider_title ): ?>
										<h2><?php echo $slider_title; ?></h2>
										<?php endif; ?>
										<?php if( $slider_descriptions ): ?>
										<p><?php echo $slider_description; ?></p>
										<?php endif; ?>
									</div>
								</div>
                                </div>
                                <div class="front-menu">                                
                                    <nav role="navigation" aria-labelledby="main navigation" class="desktop">
                                        <?php wp_nav_menu(array(
                                            'container' => false,
                                            'menu' => __( $slider_description.' Main Menu', 'bonestheme' ),
                                            'menu_class' => $slider_description.'-main-nav',
                                            'theme_location' => $slider_description.'-main-nav',
                                            'before' => '',
                                            'after' => '',
                                            'depth' => 1,
                                        )); ?>
                                    </nav>
                                </div>
							</li>
							<?php if( $slider_link ): ?>
							</a>
							<?php endif; ?>
							<?php endwhile; ?>
							<?php endif; ?>
						</ul>
					</div>
				<?php } ?>
                
					<?php if(get_field('enable_donation', 'option') == "enable") { ?>
					<div class="container give-back">
                        <div class="content">
                            <?php if(get_field('link_type', 'option') == "internal") { ?>
                            <a href="<?php the_field('donation_page', 'option'); ?>" class="btn give">
                            <?php }?>
                            <?php if(get_field('link_type', 'option') == "external") { ?>
                            <a href="<?php the_field('donation_link', 'option'); ?>" class="btn give" target="_blank">
                            <?php }?>
                               <img src="<?php echo get_template_directory_uri(); ?>/library/images/givingheart.png" alt="<?php the_field('button_text', 'option'); ?>" class="heart-logo" />
                            <?php the_field('button_text', 'option'); ?></a>
                            <?php if(get_field('supporting_text', 'option')) { ?>
                            <span><?php the_field('supporting_text', 'option'); ?></span>
                            <?php }?>
                        </div>
					</div>
					<?php }?>
			</div>
<?php 
    } else {
    // This is not the FrontPage display...
?>
			<div id="main-content" role="main">
				<?php // If set to Single/Random 
					if(get_field('hero_type', 'option') == "single") {
					$rows = get_field('hero_image'); // get all the rows
					$rand_row = $rows[ array_rand($rows) ]; // get a random row
					$silder_image = $rand_row['image']; // get the sub field value
					$slider_title = $rand_row['title']; // get the sub field value 
					$slider_description = $rand_row['description']; // get the sub field value 
					$slider_button = $rand_row['button_text']; // get the sub field value 
					$slider_link = $rand_row['link']; // get the sub field value 			
					if(!empty($silder_image)): 
						// vars
						$url = $silder_image['url'];
						$title = $silder_image['title'];
						// thumbnail
						$size = 'home-hero';
						$slide = $silder_image['sizes'][ $size ];
						$width = $silder_image['sizes'][ $size . '-width' ];
						$height = $silder_image['sizes'][ $size . '-height' ];
					endif;
				?>
				<?php if($silder_image): ?>
				<?php if($slider_link): ?>
				<a href="<?php echo $slider_link; ?>" class="hero-link">
				<?php endif; ?>
					<div id="hero" class="desktop" style="background-image: url('<?php echo $slide; ?>');">
						<div class="content <?php if($slider_title || $slider_description): ?>text<?php endif; ?>">
						<?php if($slider_title || $slider_description): ?>
							<div class="hero-description <?php the_field('vertical_text_alignment', 'option'); ?>">
								<div class="content <?php the_field('horizontal_text_alignment', 'option'); ?>">
									<?php if($slider_title): ?>
									<h2><?php echo $slider_title; ?></h2>
									<?php endif; ?>
									<?php if($slider_description): ?>
									<p><?php echo $slider_description; ?></p>
									<?php endif; ?>
									<?php if($slider_button): ?>
									<span class="btn outline"><?php echo $slider_button; ?></span>
									<?php endif; ?>
								</div>
							</div>
						<?php endif; ?>
						</div>
					</div>
				<?php if($slider_link): ?>
				</a>
				<?php endif; ?>
				<?php endif; ?>
				<?php } ?>
				
				<?php // If set to Slider
				if(get_field('hero_type', 'option') == "slider") { ?>
				<script type="text/javascript">
					jQuery("document").ready(function($) {
						$(document).ready(function(){
						  $('#bxslider').bxSlider({
						  	autoHover: true,
						  	auto: false,
						  });
						});
					});
				</script>
				<div id="slider">
					<ul id="bxslider">
						<?php if(have_rows('hero_image')): ?>
						<?php while(have_rows('hero_image')): the_row(); ?>
						<?php
							$slider_title = get_sub_field('title');
							$slider_description = get_sub_field('description');
							$slider_link = get_sub_field('link');
							$silder_image = get_sub_field('image');
							$slider_button = get_sub_field('button_text');
							if(!empty($silder_image)): 
								// vars
								$url = $silder_image['url'];
								$title = $silder_image['title'];
								// thumbnail
								$size = 'home-hero';
								$slide = $silder_image['sizes'][ $size ];
								$width = $silder_image['sizes'][ $size . '-width' ];
								$height = $silder_image['sizes'][ $size . '-height' ];
							endif;
						?>		
						<?php if($slider_link): ?>
						<a href="<?php echo $slider_link; ?>" class="hero-link">
						<?php endif; ?>
							<li style="background-image: url('<?php echo $slide; ?>');">
								<div class="bg">
									<div class="content">
										<div class="hero-description <?php the_field('vertical_text_alignment', 'option'); ?>">
											<div class="content <?php the_field('horizontal_text_alignment', 'option'); ?>">
												<?php if($slider_title): ?>
												<h2><?php echo $slider_title; ?></h2>
												<?php endif; ?>
												<?php if($slider_description): ?>
												<p><?php echo $slider_description; ?></p>
												<?php endif; ?>
												<?php if($slider_button): ?>
												<span class="btn outline"><?php echo $slider_button; ?></span>
												<?php endif; ?>
											</div>
										</div>
									</div>
								</div>
							</li>
						<?php if($slider_link): ?>
						</a>
						<?php endif; ?>
						<?php endwhile; ?>
						<?php endif; ?>
					</ul>
				</div>
				<?php } ?>
				<div class="content">
					<?php
					if(have_rows('homepage_columns')) :
						while (have_rows('homepage_columns')) : the_row();
							
							// For showing snippet from any page
							if(get_row_layout() == 'page_excerpt') 
								get_template_part('snippets/col', 'page');
					        
							// For showing list of recent post
							elseif(get_row_layout() == 'recent_posts') 
								get_template_part('snippets/col', 'posts');
							
							// For showing free form content
							elseif(get_row_layout() == 'content_block') 
								get_template_part('snippets/col', 'content');
							
							// For showing list of events from event widget
							elseif(get_row_layout() == 'upcoming_events') 
					       		get_template_part('snippets/col', 'events');
					       	
					       	// For showing a menu
					       	elseif(get_row_layout() == 'menu') 
					       		get_template_part('snippets/col', 'menu');
					       	
					       	// For showing a widget
				       		elseif( get_row_layout() == 'widget_1' ) 
				       			get_template_part('snippets/col', 'widget-1');
					       	
					       	// For showing a widget
				       		elseif( get_row_layout() == 'make_appointment' ) 
				       			get_template_part('snippets/col', 'appt');
							
						endwhile;
					endif;
			    	?>
				</div>					
			</div>
        <?php } ?>
		</div>
<?php get_footer(); ?>