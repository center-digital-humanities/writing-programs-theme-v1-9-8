<!doctype html>
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="ie lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="ie"><!--<![endif]-->
	<head>        
        <?php  // CODE to help figure out which section is displaying
            session_start();
             // This check the url and make sure the elements switch..
            $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
            $_SESSION['switchURL'] = 'wp';
            //echo $switchURL;
            if ((strpos($url,'uwc') !== false) ||(strpos($url,'wc') !== false)) {
                //echo '<h3>UWC exists.</h3>';
                $_SESSION['switchURL'] = 'uwc';
            } elseif (strpos($url,'esl') !== false) {
                //echo '<h3>ESL exists.</h3>';
                $_SESSION['switchURL'] = 'esl';
            } else {
               // echo '<h3> No ESL.</h3>';
                $_SESSION['switchURL'] = 'wp';
            }
            $switchURL = $_SESSION['switchURL'];
        ?>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php wp_title(''); ?></title>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-touch-icon_<?php echo $switchURL; ?>.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon_<?php echo $switchURL; ?>.png">
		<!--[if IE]>
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon_<?php echo $switchURL; ?>.ico">
		<![endif]-->
		<?php // set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#cc3333">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon_<?php echo $switchURL; ?>.png">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<?php wp_head(); ?>
		<?php // drop Google Analytics Here ?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-9673482-14"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-9673482-14');
        </script>
		<?php // end analytics ?>
	</head>
	<body <?php body_class(); ?>>	
		<?php if ( is_singular( 'conference' ) ) {
			get_template_part( 'top-conference' ); 
		} else {
			get_template_part( 'top' );
		} ?>